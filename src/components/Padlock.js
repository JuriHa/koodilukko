
import { useState }         from 'react';
import CodeForgot           from './CodeForgot'
import CodeInput            from './CodeInput'
import RequestSucceedWindow from './RequestSucceedWindow'

import { Button }                     from '@mui/material';
import { createTheme, ThemeProvider } from '@mui/material/styles';


function Padlock (props) {
	
	//-------------------------------------------
	//--- useState ------------------------------

	let [numInputed, setNumInputed] = useState('');
	let [requestSucceedWindow, setRequestSucceedWindow] = useState(false);
	let [DEV_on, setDEV_on] = useState(false);


	//-------------------------------------------
	//--- DATA ----------------------------------

	const nums = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 ];

	//----------------------

	const themeMain = createTheme({
		palette: {
			primary: {
				main:         '#563b8f', // See CSS "--color-violet"
				contrastText: '#fff' // See CSS "--color-white"
			},
		},
	});
	
	
	//-------------------------------------------
	//--- METHODS -------------------------------

	function numInputed_more (e) {
		if (numInputed.length < 4) setNumInputed(numInputed += e.target.value.toString());
		if (numInputed.length === 4) submit_main();
	}

	//----------------------

	function numInputed_less () {
		setNumInputed(numInputed.slice(0, -1));
	}

	//----------------------

	function submit_main () {

		if (DEV_on) {
			DEV_submit_main();
		}
		else {
			fetch('xxx', {
				method: 'POST',
				headers: { "Content-Type": "application/json" },
				body: JSON.stringify(numInputed)
			}).then( response => { if (response.status === 200) requestSucceedWindow_show(); }
				// TODO: add reaction to the Server response
				// ... after Server is connected.
			).catch(error => {
				requestFailed();
        	});
		}
	}

	function requestSucceedWindow_show () {
		setRequestSucceedWindow(true);
	}

	//-----------------------

	function requestSucceed () {
		setRequestSucceedWindow(false); // SHOW window
		setNumInputed(''); // RESET it
	}

	function requestFailed () {
		alert('Koodisi on väärin. Kokeile uudelleen');
	}


	//--- DEV -------------------

	function DEV_submit_main () {
		(numInputed === '1234') ? requestSucceedWindow_show() : requestFailed();
	}

	//--- DEV -------------------
	function DEV_on_set () {
		setDEV_on(!DEV_on);
		setNumInputed(''); // RESET it
	}


	//-------------------------------------------
	//--- VIEW ----------------------------------

	return (

		<div className="padlock">


			{
				requestSucceedWindow &&
				<RequestSucceedWindow
					themeMain      = { themeMain }
					requestSucceed = { requestSucceed } />
			}


            <CodeInput
            	themeMain       = {themeMain}
            	numInputed      = {numInputed}
            	numInputed_less = {numInputed_less} />


			<div className="-nums">
				<ThemeProvider theme={themeMain}>
		  			{nums.map(
		  				(num) =>
		  					<Button variant = "contained"
		  					        color   = "primary"
		  					        value   = { num }
		  					        key     = { 'padlock-num-' + num }
		  					        onClick = { numInputed_more }
		  					        fullWidth>{ num }</Button>
		  			)}
		  		</ThemeProvider>
			</div>


            <CodeForgot themeMain={themeMain} />


            <div className="DEV_request-succeed-window-box-show">
				<ThemeProvider theme={themeMain}>
            		<Button variant = "outlined"
            				color   = "secondary"
            				size    = "small"
            				onClick={ DEV_on_set }>(DEV) request response imitation: { DEV_on ? 'on' : 'off' }</Button>
		  		</ThemeProvider>
            </div>

		
		</div>


	);

}

export default Padlock;