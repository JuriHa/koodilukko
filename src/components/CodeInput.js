

import { useState } from 'react';

import   BackspaceOutlinedIcon                   from '@mui/icons-material/BackspaceOutlined';
import { TextField, InputAdornment, IconButton } from '@mui/material';
import { Visibility, VisibilityOff }             from '@mui/icons-material';
import { ThemeProvider }                         from '@mui/material/styles';



function CodeInput (props) {
	
	
	//-------------------------------------------
	//--- useState ------------------------------

	const [passwordShow, setPasswordShow] = useState(false);
    const passwordShow_toggle = () => setPasswordShow(!passwordShow);


	//-------------------------------------------
	//--- DATA ----------------------------------

	// let numInputed = '';

	
	//-------------------------------------------
	//--- METHODS --------------------------------

	// function xxx (e) {
	// 	numInputed = props.numInputed;
	// }


	//-------------------------------------------
	//--- VIEW ----------------------------------

	return (


		<div className="code-input">
			<ThemeProvider theme={props.themeMain}>

				<TextField
					fullWidth
					id         = "outlined-basic"
					label      = "Koodi"
					variant    = "outlined"
					size       = "small"
					value      = { props.numInputed }
					type       = { passwordShow ? 'text' : 'password' }
					inputProps = {{ readOnly: true }}
					InputProps = {{ endAdornment: (
						<InputAdornment position="end">
							<IconButton aria-label="toggle password visibility" onClick={passwordShow_toggle} onMouseDown={passwordShow_toggle}>
								{ passwordShow ? <Visibility /> : <VisibilityOff /> }
							</IconButton>
						</InputAdornment>
					)}}
				/>

		    	<IconButton
		    		aria-label = "Backspace"
		    		color      = "primary"
		    		onClick    = { props.numInputed_less }>
		    		<BackspaceOutlinedIcon />
		    	</IconButton>

	  		</ThemeProvider>
		</div>


	);

}

export default CodeInput;