
import { Button }        from '@mui/material';
import { ThemeProvider } from '@mui/material/styles';


function RequestSucceedWindow (props) {

	//-------------------------------------------
	//--- DATA ----------------------------------
	// const numInputed = '';
	
	
	//-------------------------------------------
	//--- METHODS --------------------------------
	// function num_get (e) { alert(e.target.value); }


	//-------------------------------------------
	//--- VIEW ----------------------------------

	return (

		<div class="request-succeed-window">
			<div className="-box">
				<div><h1>Koodisi on oikein!</h1></div>
				<div>Voit jatkaa nyt</div>
				<div>
					<ThemeProvider theme={props.themeMain}>
						<Button
							variant = "contained"
			  				color   = "primary"
				   		    onClick = { props.requestSucceed }>OK</Button>
				    </ThemeProvider>
				</div>
			</div>
		</div>


	);

}

export default RequestSucceedWindow;