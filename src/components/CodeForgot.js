
import { Tooltip, Button } from '@mui/material';
import { ThemeProvider }   from '@mui/material/styles';


function CodeForgot (props) {

	//-------------------------------------------
	//--- DATA ----------------------------------
	// const numInputed = '';
	
	
	//-------------------------------------------
	//--- METHODS --------------------------------
	// function num_get (e) { alert(e.target.value); }


	//-------------------------------------------
	//--- VIEW ----------------------------------

	return (

		<div className="code-forgot">
			<ThemeProvider theme={props.themeMain}>
				<Tooltip title="Kokeile 1234" arrow>
					<Button size="small">Unohditko koodisi?</Button>
				</Tooltip>
			</ThemeProvider>
		</div>

	);

}

export default CodeForgot;