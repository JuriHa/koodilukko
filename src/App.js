import Padlock from './components/Padlock';



function App () {
  
  //-------------------------------------------
  //--- VIEW ----------------------------------
  
  return (

    <div className="container_main">

      <div className="-title">
        <h1>Koodilukko</h1>
      </div>

      <p className="-desc">Syötä nelja numeroa</p>
      
      <Padlock />

    </div>

  );

}

export default App;
